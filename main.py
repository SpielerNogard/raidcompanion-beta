from kivymd.app import MDApp
from kivy.lang import Builder
from kivymd.uix.list import ImageLeftWidget, OneLineAvatarListItem, MDList,ThreeLineAvatarListItem
from kivy.uix.scrollview import ScrollView
from kivy.network.urlrequest import UrlRequest
from kivy.uix.image import AsyncImage
from kivymd.uix.label import MDLabel
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget
from kivymd.uix.button import MDRectangleFlatButton 
from kivymd.uix.dialog import MDDialog



from Vairables import Pokemon_Liste, screen_helper, Raidanmeldung_helper,Raidanmeldung_helper2,Raidanmeldung_helper3

class MainApp(MDApp):
    def build(self):
        self.Server = "http://5.39.56.148:5000/"
        self.theme_cls.primary_palette = "Green"
        self.theme_cls.primary_hue = "100"
        self.theme_cls.theme_style = "Dark"
        self.gymdetails = UrlRequest(self.Server+"gyminformation")
        self.i = 0

        self.Raids_LVL_1_1 = []
        self.Raids_LVL_2_1 = []
        self.Raids_LVL_3_1 = []
        self.Raids_LVL_4_1 = []
        self.Raids_LVL_5_1 = []
        self.Raids_LVL_6_1 = []

        self.alle_Raids = UrlRequest(self.Server+"Raids",on_success=self.habe_daten)
        screen = Builder.load_string(screen_helper)
        return(screen)
    
    def on_start(self):
        self.paint_navigation()
    
    def habe_daten(self,a,abs):
        for a in self.alle_Raids.result:
            level = a[1]
            if level == 1:
                self.Raids_LVL_1_1.append(a)
            if level == 2:
                self.Raids_LVL_2_1.append(a)
            if level == 3:
                self.Raids_LVL_3_1.append(a)
            if level == 4:
                self.Raids_LVL_4_1.append(a)
            if level == 5:
                self.Raids_LVL_5_1.append(a)
            if level == 6:
                self.Raids_LVL_6_1.append(a)
        self.zeige_liste_für_aktive_raids()
    def zeige_liste_für_aktive_raids(self):

        Bild_Pfad = "1.png"
        image = ImageLeftWidget(source = Bild_Pfad)
        items = OneLineAvatarListItem(text="Raids LVL 1", on_press = self.change_screens_to_lvl1)
        items.add_widget(image)
        self.root.ids.nav_list.add_widget(items)

        Bild_Pfad = "1.png"
        image = ImageLeftWidget(source = Bild_Pfad)
        items = OneLineAvatarListItem(text="Raids LVL 2", on_press = self.change_screens_to_lvl2)
        items.add_widget(image)
        self.root.ids.nav_list.add_widget(items)

        Bild_Pfad = "2.png"
        image = ImageLeftWidget(source = Bild_Pfad)
        items = OneLineAvatarListItem(text="Raids LVL 3", on_press = self.change_screens_to_lvl3)
        items.add_widget(image)
        self.root.ids.nav_list.add_widget(items)

        Bild_Pfad = "2.png"
        image = ImageLeftWidget(source = Bild_Pfad)
        items = OneLineAvatarListItem(text="Raids LVL 4", on_press = self.change_screens_to_lvl4)
        items.add_widget(image)
        self.root.ids.nav_list.add_widget(items)

        Bild_Pfad = "5.png"
        image = ImageLeftWidget(source = Bild_Pfad)
        items = OneLineAvatarListItem(text="Raids LVL 5", on_press = self.change_screens_to_lvl5)
        items.add_widget(image)
        self.root.ids.nav_list.add_widget(items)

        Bild_Pfad = "6.png"
        image = ImageLeftWidget(source = Bild_Pfad)
        items = OneLineAvatarListItem(text="Raids LVL 6", on_press = self.change_screens_to_lvl6)
        items.add_widget(image)
        self.root.ids.nav_list.add_widget(items)

        Bild_Pfad = "mew.png"
        image = ImageLeftWidget(source = Bild_Pfad)
        items = OneLineAvatarListItem(text="Aktive Raids",on_press = self.change_to_active_raid_screen)
        items.add_widget(image)
        self.root.ids.nav_list.add_widget(items)

    def paint_navigation(self):
        Bild_Pfad = "poki.png"
        image = ImageLeftWidget(source = Bild_Pfad)
        items = OneLineAvatarListItem(text="Startseite", on_press = self.change_screens_to_home)
        items.add_widget(image)
        self.root.ids.nav_list.add_widget(items)

        

    def change_screens_to_home(self,instance):
        self.root.ids.HauptToolbar.title = "RaidCompanion"
        self.root.ids.screen_manager.current = "HauptScreen"

    def change_screens_to_lvl1(self,instance):
        self.root.ids.HauptToolbar.title = "RaidsLVL1"
        self.root.ids.screen_manager.current = "RaidsLVL1"
        self.Raids_LVL_1 = UrlRequest(self.Server+"Raidslvl1", on_success=self.have_Raids1)

    def change_screens_to_lvl2(self,instance):
        self.root.ids.HauptToolbar.title = "RaidsLVL2"
        self.root.ids.screen_manager.current = "RaidsLVL2"
        self.Raids_LVL_2 = UrlRequest(self.Server+"Raidslvl2", on_success=self.have_Raids2)

    def change_screens_to_lvl3(self,instance):
        self.root.ids.HauptToolbar.title = "RaidsLVL3"
        self.root.ids.screen_manager.current = "RaidsLVL3"
        self.Raids_LVL_3 = UrlRequest(self.Server+"Raidslvl3", on_success=self.have_Raids3)

    def change_screens_to_lvl4(self,instance):
        self.root.ids.HauptToolbar.title = "RaidsLVL4"
        self.root.ids.screen_manager.current = "RaidsLVL4"
        self.Raids_LVL_4 = UrlRequest(self.Server+"Raidslvl4", on_success=self.have_Raids4)

    def change_screens_to_lvl5(self,instance):
        self.root.ids.HauptToolbar.title = "RaidsLVL5"
        self.root.ids.screen_manager.current = "RaidsLVL5"
        self.Raids_LVL_5 = UrlRequest(self.Server+"Raidslvl5", on_success=self.have_Raids5)

    def change_screens_to_lvl6(self,instance):
        self.root.ids.HauptToolbar.title = "RaidsLVL6"
        self.root.ids.screen_manager.current = "RaidsLVL6"
        self.Raids_LVL_6 = UrlRequest(self.Server+"Raidslvl6", on_success=self.have_Raids6)

    def change_to_raid_screen(self,instance):
        self.root.ids.screen_manager.current = "Raideintragung"
        self.raid_screen(instance.text,instance.secondary_text,instance.id)

    def change_to_raid_screen_active(self,instance):
        self.root.ids.screen_manager.current = "Raideintragung"
        self.raid_screen_active(instance.text,instance.secondary_text,instance.id)

    def change_to_active_raid_screen(self,instance):
        self.root.ids.screen_manager.current = "aktiveRaids"
        self.root.ids.HauptToolbar.title = "aktiveRaids"
        self.aktive_Raids_Abfrage = UrlRequest(self.Server+"raidanmeldungen", on_success=self.zeige_active_raids)
        #self.zeige_active_raids()

    def zeige_active_raids(self,a,args):
        alle_LVL1 = []
        list_view = MDList()
        scroll = ScrollView()
        scroll.add_widget(list_view)
        for a in self.aktive_Raids_Abfrage.result:
            Arena_Name = a[0]
            endet= a[1]
            nah= a[2]
            fern= a[3]
            einladung= a[4]
            aktiv= a[5]
            
            Bild_Pfad = Arena_Name+".png"
            Bild_Pfad = Bild_Pfad.replace('"','')
            Bild_Pfad = Bild_Pfad.replace(' / ','')
                
            Bild_Pfad = Bild_Pfad.replace('ü','ue')
            Bild_Pfad = Bild_Pfad.replace('ö','oe')
            Bild_Pfad = Bild_Pfad.replace('ä','ae')
            Bild_Pfad = Bild_Pfad.replace('Ü','Ue')
            Bild_Pfad = Bild_Pfad.replace('Ö','Oe')
            Bild_Pfad = Bild_Pfad.replace('Ä','Ae')
            image = ImageLeftWidget(source = Bild_Pfad)
            Ergebnis= self.get_raid_by_name(Arena_Name)
            
            pokemon_id = Ergebnis[4]
            if pokemon_id == None:
                Pokemon = "Egg"
            else:
                pokemon_id = pokemon_id-1
                Pokemon_bild = Pokemon_Liste[pokemon_id]
                Pokemon = Pokemon_bild[0]
            items = ThreeLineAvatarListItem(text = Arena_Name, secondary_text = "Raid Level: "+str(Ergebnis[1]), tertiary_text = "Pokemon: "+str(Pokemon),id = Ergebnis[5] ,on_press = self.change_to_raid_screen_active)
            items.add_widget(image)
            list_view.add_widget(items)
        self.root.ids.aktiveRaids.clear_widgets()
        self.root.ids.aktiveRaids.add_widget(scroll)  
        
    def raid_screen(self,Arena_name,level,gym_id):
        level = int(level.replace("Raid Level: ",""))
        self.root.ids.HauptToolbar.title = Arena_name
        self.Arena = Arena_name
        Box = BoxLayout(orientation = 'vertical')
        list_view = MDList()
        if level == 1:
            Raids_LVL1 = self.Raids_LVL_1.result
        if level == 2:
            Raids_LVL1 = self.Raids_LVL_2.result
        if level == 3:
            Raids_LVL1 = self.Raids_LVL_3.result
        if level == 4:
            Raids_LVL1 = self.Raids_LVL_4.result
        if level == 5:
            Raids_LVL1 = self.Raids_LVL_5.result
        if level == 6:
            Raids_LVL1 = self.Raids_LVL_6.result   
        for a in Raids_LVL1:
                if gym_id in a:
                    Gym_id = a[0]
                    Raid_lvl = a[1]
                    spawn = a[2]
                    start = a[3]
                    end= a[4]
                    pokemon_id= a[5]
                    cp= a[6]
                    move1= a[7]
                    move2= a[8]
                    last_scanned= a[9]
                    form= a[10]
                    is_exclusice= a[11]
                    gender= a[12]
                    costume= a[13]
                    evolution= a[14]
                    if pokemon_id == None:
                        Pokemon = "Egg"
                        Bild = "1.png"
                    else:
                        real_pokemonid = pokemon_id - 1
                        Pokemon_Bild = Pokemon_Liste[real_pokemonid]
                        Pokemon = Pokemon_Bild[0]
                        Bild = Pokemon_Bild[1]

                    img = AsyncImage(source = Bild, allow_stretch = True, size = (100,100))
                    Pokemon_Name = MDLabel(text = Pokemon, halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                    CP = MDLabel(text = "CP: "+str(cp), halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                    Start = MDLabel(text = "Schlupf: "+str(start), halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                    Ende = MDLabel(text = "Ende: "+str(end), halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                    btn1 = MDRectangleFlatButton(text = "Teilnehmen", pos_hint = {'center_x':0.5, 'center_y':0.5},id = gym_id,on_release = self.Raid_anmeldung)
                    Platzhalter = Widget()

                    
        Box.add_widget(img)
        Box.add_widget(Pokemon_Name)
        Box.add_widget(CP)
        Box.add_widget(Start)
        Box.add_widget(Ende)
        Box.add_widget(btn1)
        Box.add_widget(Platzhalter)

        self.root.ids.Raideintragung.clear_widgets()
        self.root.ids.Raideintragung.add_widget(Box)
    
    def raid_screen_active(self,Arena_name,level,gym_id):
        level = int(level.replace("Raid Level: ",""))
        self.root.ids.HauptToolbar.title = Arena_name
        self.Arena = Arena_name
        scrollen = ScrollView()
        Box = BoxLayout(orientation = 'vertical')
        list_view = MDList()
        if level == 1:
            Raids_LVL1 = self.Raids_LVL_1_1
        if level == 2:
            Raids_LVL1 = self.Raids_LVL_2_1
        if level == 3:
            Raids_LVL1 = self.Raids_LVL_3_1
        if level == 4:
            Raids_LVL1 = self.Raids_LVL_4_1
        if level == 5:
            Raids_LVL1 = self.Raids_LVL_5_1
        if level == 6:
            Raids_LVL1 = self.Raids_LVL_6_1   
        for a in Raids_LVL1:
                if gym_id in a:
                    Gym_id = a[0]
                    Raid_lvl = a[1]
                    spawn = a[2]
                    start = a[3]
                    end= a[4]
                    pokemon_id= a[5]
                    cp= a[6]
                    move1= a[7]
                    move2= a[8]
                    last_scanned= a[9]
                    form= a[10]
                    is_exclusice= a[11]
                    gender= a[12]
                    costume= a[13]
                    evolution= a[14]
                    if pokemon_id == None:
                        Pokemon = "Egg"
                        Bild = "1.png"
                    else:
                        real_pokemonid = pokemon_id - 1
                        Pokemon_Bild = Pokemon_Liste[real_pokemonid]
                        Pokemon = Pokemon_Bild[0]
                        Bild = Pokemon_Bild[1]

                    img = AsyncImage(source = Bild, allow_stretch = True, size = (100,100))
                    Pokemon_Name = MDLabel(text = Pokemon, halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                    CP = MDLabel(text = "CP: "+str(cp), halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                    Start = MDLabel(text = "Schlupf: "+str(start), halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                    Ende = MDLabel(text = "Ende: "+str(end), halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                    btn1 = MDRectangleFlatButton(text = "Teilnehmen", pos_hint = {'center_x':0.5, 'center_y':0.5},id = gym_id,on_release = self.Raid_anmeldung)
                    Platzhalter = Widget()
        
        Box.add_widget(img)
        Box.add_widget(Pokemon_Name)
        Box.add_widget(CP)
        Box.add_widget(Start)
        Box.add_widget(Ende)
        Box.add_widget(btn1)
        #Box.add_widget(Platzhalter)
        for b in self.aktive_Raids_Abfrage.result:
            print(b)
            Arena_name2 = b[0]
            print(Arena_name2)
            endet2= b[1]
            nah2= b[2]
            fern2= b[3]
            einladung2= b[4]
            aktiv2= b[5]

            if Arena_name2 == Arena_name:
                print(Arena_name2)
                CP = MDLabel(text = "Nah:", halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                Box.add_widget(CP)
                for c in nah2:
                    Trainer_Name = c[0]
                    Trainer_zusatz = c[1]
                    text1 = Trainer_Name+" +"+str(Trainer_zusatz)
                    label1 = MDLabel(text = text1, halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                    Box.add_widget(label1)
                CP2 = MDLabel(text = "Fern:", halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                Box.add_widget(CP2)
                for c in fern2:
                    Trainer_Name = c[0]
                    Trainer_zusatz = c[1]
                    text1 = Trainer_Name+" +"+str(Trainer_zusatz)
                    label1 = MDLabel(text = text1, halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                    Box.add_widget(label1) 
                CP3 = MDLabel(text = "Einladung:", halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                Box.add_widget(CP3)
                for c in einladung2:
                    Trainer_Name = c[0]
                    Trainer_zusatz = c[1]
                    text1 = Trainer_Name+" +"+str(Trainer_zusatz)
                    label1 = MDLabel(text = text1, halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
                    Box.add_widget(label1) 



        scrollen.add_widget(Box)
        self.root.ids.Raideintragung.clear_widgets()
        self.root.ids.Raideintragung.add_widget(scrollen)
    
    def Raid_anmeldung(self,instance):
        self.root.ids.Raidanmeldung.clear_widgets()
        self.root.ids.HauptToolbar.title = "Raid Anmeldung"
        self.root.ids.screen_manager.current = "Raidanmeldung"

        self.username = Builder.load_string(Raidanmeldung_helper)
        self.Nebenaccounts = Builder.load_string(Raidanmeldung_helper2)
        self.Nah = Builder.load_string(Raidanmeldung_helper3)

        btn1 = MDRectangleFlatButton(text="Anmelden",pos_hint={'center_x':0.5,'center_y':0.5}, on_release = self.show_data)

        Box2 = ScrollView()
        Box = BoxLayout(orientation = 'vertical')
        Box.add_widget(self.username)
        Box.add_widget(self.Nebenaccounts)
        Box.add_widget(self.Nah)
        Box.add_widget(btn1)
        CP = MDLabel(text = "", halign = 'center', theme_text_color = 'Custom',text_color = (236/255.0,98/255.0,81/255.0,1), font_style = 'Caption')
        Box.add_widget(CP)
        Box2.add_widget(Box)
        self.root.ids.Raidanmeldung.add_widget(Box2)

    def go_home(self,obj):
        self.change_screens_to_home(obj)
        self.dialog.dismiss()

    def go_home_error(self,i,args):
        self.change_screens_to_home(i)
    def show_data(self,obj):
        btn1 = MDRectangleFlatButton(text="Ok",pos_hint={'center_x':0.5,'center_y':0.5}, on_release = self.go_home)
        self.dialog=MDDialog(title = "Erfolg",text = "Du hast dich erfolgreich eingetragen. Der Fortschritt ist unter 'Aktive Raids' zu finden",size_hint = (0.7,1), buttons = [btn1])
        self.anmelden(self.Arena,self.username.text,self.Nebenaccounts.text,self.Nah.text)

    def erfolgreiche_Anmeldung(self,i,args):
        self.dialog.open()

    def anmelden(self,Arena,Username,Nebenaccounts,Art):
        Arena = Arena.replace('Ö','Oe')
        Arena = Arena.replace('ö','oe')
        Arena = Arena.replace('Ä','Ae')
        Arena = Arena.replace('ä','ae')
        Arena = Arena.replace('Ü','Ue')
        Arena = Arena.replace('ü','ue')

        Username = Username.replace('Ö','Oe')
        Username = Username.replace('ö','oe')
        Username = Username.replace('Ä','Ae')
        Username = Username.replace('ä','ae')
        Username = Username.replace('Ü','Ue')
        Username = Username.replace('ü','ue')

        Link = "raidanmeldung?arena="+str(Arena)+"&username="+str(Username)+"&anzahl="+str(Nebenaccounts)+"&nah="+str(Art)
        Link = Link.replace(" ","%20")
        print(Link)
        self.aktuelle_Anmeldung = UrlRequest(self.Server+Link, on_success=self.erfolgreiche_Anmeldung, on_failure=self.go_home_error)

    def have_Raids1(self,a,args):
        alle_LVL1 = []
        list_view = MDList()
        scroll = ScrollView()
        scroll.add_widget(list_view)
        for a in self.Raids_LVL_1.result:
            Ergebnis = self.get_gyminformation_for_raids(a)
            alle_LVL1.append(Ergebnis)
        for b in alle_LVL1:
            Name_Arena = b[0]
            Arena_bild = b[1]
            Level = b[2]
            Pokemon_Bild = b[3]
            Pokemon = Pokemon_Bild[0]
            Url = Pokemon_Bild[1]
            gym_id = b[4]
            
            Bild_Pfad = Name_Arena+".png"
            Bild_Pfad = Bild_Pfad.replace('"','')
            Bild_Pfad = Bild_Pfad.replace(' / ','')
                
            Bild_Pfad = Bild_Pfad.replace('ü','ue')
            Bild_Pfad = Bild_Pfad.replace('ö','oe')
            Bild_Pfad = Bild_Pfad.replace('ä','ae')
            Bild_Pfad = Bild_Pfad.replace('Ü','Ue')
            Bild_Pfad = Bild_Pfad.replace('Ö','Oe')
            Bild_Pfad = Bild_Pfad.replace('Ä','Ae')
            image = ImageLeftWidget(source = Bild_Pfad)
            items = ThreeLineAvatarListItem(text = Name_Arena, secondary_text = "Raid Level: "+str(Level), tertiary_text = "Pokemon: "+str(Pokemon),id = gym_id ,on_press = self.change_to_raid_screen)
            items.add_widget(image)
            list_view.add_widget(items)
        self.root.ids.RaidsLVL1.clear_widgets()
        self.root.ids.RaidsLVL1.add_widget(scroll)

    def have_Raids2(self,a,args):
        alle_LVL1 = []
        list_view = MDList()
        scroll = ScrollView()
        scroll.add_widget(list_view)
        for a in self.Raids_LVL_2.result:
            Ergebnis = self.get_gyminformation_for_raids(a)
            alle_LVL1.append(Ergebnis)
        for b in alle_LVL1:
            Name_Arena = b[0]
            Arena_bild = b[1]
            Level = b[2]
            Pokemon_Bild = b[3]
            Pokemon = Pokemon_Bild[0]
            Url = Pokemon_Bild[1]
            gym_id = b[4]
            
            Bild_Pfad = Name_Arena+".png"
            Bild_Pfad = Bild_Pfad.replace('"','')
            Bild_Pfad = Bild_Pfad.replace(' / ','')
                
            Bild_Pfad = Bild_Pfad.replace('ü','ue')
            Bild_Pfad = Bild_Pfad.replace('ö','oe')
            Bild_Pfad = Bild_Pfad.replace('ä','ae')
            Bild_Pfad = Bild_Pfad.replace('Ü','Ue')
            Bild_Pfad = Bild_Pfad.replace('Ö','Oe')
            Bild_Pfad = Bild_Pfad.replace('Ä','Ae')
            image = ImageLeftWidget(source = Bild_Pfad)
            items = ThreeLineAvatarListItem(text = Name_Arena, secondary_text = "Raid Level: "+str(Level), tertiary_text = "Pokemon: "+str(Pokemon),id = gym_id, on_press = self.change_to_raid_screen)
            items.add_widget(image)
            list_view.add_widget(items)
        self.root.ids.RaidsLVL2.clear_widgets()
        self.root.ids.RaidsLVL2.add_widget(scroll)

    def have_Raids3(self,a,args):
        alle_LVL1 = []
        list_view = MDList()
        scroll = ScrollView()
        scroll.add_widget(list_view)
        for a in self.Raids_LVL_3.result:
            Ergebnis = self.get_gyminformation_for_raids(a)
            alle_LVL1.append(Ergebnis)
        for b in alle_LVL1:
            Name_Arena = b[0]
            Arena_bild = b[1]
            Level = b[2]
            Pokemon_Bild = b[3]
            Pokemon = Pokemon_Bild[0]
            Url = Pokemon_Bild[1]
            gym_id = b[4]
            
            Bild_Pfad = Name_Arena+".png"
            Bild_Pfad = Bild_Pfad.replace('"','')
            Bild_Pfad = Bild_Pfad.replace(' / ','')
                
            Bild_Pfad = Bild_Pfad.replace('ü','ue')
            Bild_Pfad = Bild_Pfad.replace('ö','oe')
            Bild_Pfad = Bild_Pfad.replace('ä','ae')
            Bild_Pfad = Bild_Pfad.replace('Ü','Ue')
            Bild_Pfad = Bild_Pfad.replace('Ö','Oe')
            Bild_Pfad = Bild_Pfad.replace('Ä','Ae')
            image = ImageLeftWidget(source = Bild_Pfad)
            items = ThreeLineAvatarListItem(text = Name_Arena, secondary_text = "Raid Level: "+str(Level), tertiary_text = "Pokemon: "+str(Pokemon),id = gym_id,on_press = self.change_to_raid_screen)
            items.add_widget(image)
            list_view.add_widget(items)
        self.root.ids.RaidsLVL3.clear_widgets()
        self.root.ids.RaidsLVL3.add_widget(scroll)

    def have_Raids4(self,a,args):
        alle_LVL1 = []
        list_view = MDList()
        scroll = ScrollView()
        scroll.add_widget(list_view)
        for a in self.Raids_LVL_4.result:
            Ergebnis = self.get_gyminformation_for_raids(a)
            alle_LVL1.append(Ergebnis)
        for b in alle_LVL1:
            Name_Arena = b[0]
            Arena_bild = b[1]
            Level = b[2]
            Pokemon_Bild = b[3]
            Pokemon = Pokemon_Bild[0]
            Url = Pokemon_Bild[1]
            gym_id = b[4]
            
            Bild_Pfad = Name_Arena+".png"
            Bild_Pfad = Bild_Pfad.replace('"','')
            Bild_Pfad = Bild_Pfad.replace(' / ','')
                
            Bild_Pfad = Bild_Pfad.replace('ü','ue')
            Bild_Pfad = Bild_Pfad.replace('ö','oe')
            Bild_Pfad = Bild_Pfad.replace('ä','ae')
            Bild_Pfad = Bild_Pfad.replace('Ü','Ue')
            Bild_Pfad = Bild_Pfad.replace('Ö','Oe')
            Bild_Pfad = Bild_Pfad.replace('Ä','Ae')
            image = ImageLeftWidget(source = Bild_Pfad)
            items = ThreeLineAvatarListItem(text = Name_Arena, secondary_text = "Raid Level: "+str(Level), tertiary_text = "Pokemon: "+str(Pokemon),id = gym_id,on_press = self.change_to_raid_screen)
            items.add_widget(image)
            list_view.add_widget(items)
        self.root.ids.RaidsLVL4.clear_widgets()
        self.root.ids.RaidsLVL4.add_widget(scroll)

    def have_Raids5(self,a,args):
        alle_LVL1 = []
        list_view = MDList()
        scroll = ScrollView()
        scroll.add_widget(list_view)
        for a in self.Raids_LVL_5.result:
            Ergebnis = self.get_gyminformation_for_raids(a)
            alle_LVL1.append(Ergebnis)
        for b in alle_LVL1:
            Name_Arena = b[0]
            Arena_bild = b[1]
            Level = b[2]
            Pokemon_Bild = b[3]
            Pokemon = Pokemon_Bild[0]
            Url = Pokemon_Bild[1]
            gym_id = b[4]

            
            Bild_Pfad = Name_Arena+".png"
            Bild_Pfad = Bild_Pfad.replace('"','')
            Bild_Pfad = Bild_Pfad.replace(' / ','')
                
            Bild_Pfad = Bild_Pfad.replace('ü','ue')
            Bild_Pfad = Bild_Pfad.replace('ö','oe')
            Bild_Pfad = Bild_Pfad.replace('ä','ae')
            Bild_Pfad = Bild_Pfad.replace('Ü','Ue')
            Bild_Pfad = Bild_Pfad.replace('Ö','Oe')
            Bild_Pfad = Bild_Pfad.replace('Ä','Ae')
            image = ImageLeftWidget(source = Bild_Pfad)
            items = ThreeLineAvatarListItem(text = Name_Arena, secondary_text = "Raid Level: "+str(Level), tertiary_text = "Pokemon: "+str(Pokemon),id = gym_id,on_press = self.change_to_raid_screen)
            items.add_widget(image)
            list_view.add_widget(items)
        self.root.ids.RaidsLVL5.clear_widgets()
        self.root.ids.RaidsLVL5.add_widget(scroll)

    def have_Raids6(self,a,args):
        alle_LVL1 = []
        list_view = MDList()
        scroll = ScrollView()
        scroll.add_widget(list_view)
        for a in self.Raids_LVL_6.result:
            Ergebnis = self.get_gyminformation_for_raids(a)
            alle_LVL1.append(Ergebnis)
        for b in alle_LVL1:
            Name_Arena = b[0]
            Arena_bild = b[1]
            Level = b[2]
            Pokemon_Bild = b[3]
            Pokemon = Pokemon_Bild[0]
            Url = Pokemon_Bild[1]
            gym_id = b[4]
            
            Bild_Pfad = Name_Arena+".png"
            Bild_Pfad = Bild_Pfad.replace('"','')
            Bild_Pfad = Bild_Pfad.replace(' / ','')
                
            Bild_Pfad = Bild_Pfad.replace('ü','ue')
            Bild_Pfad = Bild_Pfad.replace('ö','oe')
            Bild_Pfad = Bild_Pfad.replace('ä','ae')
            Bild_Pfad = Bild_Pfad.replace('Ü','Ue')
            Bild_Pfad = Bild_Pfad.replace('Ö','Oe')
            Bild_Pfad = Bild_Pfad.replace('Ä','Ae')
            image = ImageLeftWidget(source = Bild_Pfad)
            items = ThreeLineAvatarListItem(text = Name_Arena, secondary_text = "Raid Level: "+str(Level), tertiary_text = "Pokemon: "+str(Pokemon),id = gym_id,on_press = self.change_to_raid_screen)
            items.add_widget(image)
            list_view.add_widget(items)
        self.root.ids.RaidsLVL6.clear_widgets()
        self.root.ids.RaidsLVL6.add_widget(scroll)

    def get_raid_by_name(self,name):

        for a in self.gymdetails.result:
            if name in a:
                gym_id = a[0]
                Arena_name = a[1]
                Beschreibung = a[2]
                Bild_url = a[3]
                last_scanned = a[4]

        for a in self.alle_Raids.result:
            if gym_id in a:
                level = a[1]
                start  = a[2]
                schlupf = a[3]
                ende = a[4]
                pokemon_id = a[5]
                cp = a[6]

        Ergebnis = []
        Ergebnis.append(Arena_name)
        Ergebnis.append(level)
        Ergebnis.append(schlupf)
        Ergebnis.append(ende)
        Ergebnis.append(pokemon_id)
        Ergebnis.append(gym_id)
        return(Ergebnis)





    def get_gyminformation_for_raids(self,Raids):
        Ausgabe = []
        a = Raids
        gym_id = a[0]
        level = a[1]
        spawn = a[2]
        start = a[3]
        end = a[4]
        pokemon_id = a[5]
        cp = a[6]
        move_1 = a[7]
        move_2 = a[8]
        last_scanned = a[9]
        form  = a[10]
        is_exclusive = a[11]
        gender = a[12]
        costume = a[13]
        evolutions = a[14]
        for b in self.gymdetails.result:
            name = b[1]
            url = b[3]
            if gym_id in b:
                Ausgabe.append(name)
                Ausgabe.append(url)
                Ausgabe.append(level)
                if pokemon_id != None:
                    Poki_Bild = Pokemon_Liste[pokemon_id - 1]
                    Ausgabe.append(Poki_Bild)
                else:
                    Sonst = ["Egg",None]
                    Ausgabe.append(Sonst)
                Ausgabe.append(gym_id)
        return(Ausgabe)


        


MainApp().run()
